import { Component, OnInit } from '@angular/core';
import { DataService } from '../../service/data.service';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-contactos',
  standalone: true,
  imports: [RouterLink],
  providers:[DataService],
  templateUrl: './contactos.component.html',
  styleUrl: './contactos.component.css'
})
export class ContactosComponent{

  contactos:any;
  id:Number = 0;

  constructor(private dataService:DataService){}

  ngOnInit() {
    this.getContactos();
  }

  //Obtengo todos los contactos
  getContactos(){
    this.dataService.getData('/api/contacto').subscribe(data => {
      // console.log(this.contactos=data);
      this.contactos=data;
      this.contactos = this.contactos.contactos;
    });
  }

  getId(id:Number){
    this.id = id;
  }

  // Eliminacion de contactos
  eliminarContacto(id:Number){
    console.log(this.id);
    if(confirm('Seguro que desea eliminar este contacto?')){
      this.dataService.deleteData('/api/contacto/' + id, {}).subscribe(data=>{
        var result:any = data;
        alert(result.message);
        window.location.reload();
      });

    }
  }
}
