import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { DataService } from '../../service/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-formulario',
  standalone: true,
  imports: [ReactiveFormsModule],
  providers:[DataService],
  templateUrl: './formulario.component.html',
  styleUrl: './formulario.component.css'
})
export class FormularioComponent implements OnInit {

  formularioContacto:FormGroup;
  formularioTelefono:FormGroup;
  formularioEmail:FormGroup;
  // formularioDireccion:FormGroup;
  contacto:any;
  @Input() id = '';

  constructor(private form: FormBuilder,private dataService:DataService,private router: Router){
    if(this.id){
      this.dataService.getData('/api/contacto/' + this.id).subscribe(data => {
        this.contacto = data;
        this.contacto = this.contacto.contacto;
        
        console.log(this.contacto);

      });
      this.formularioContacto = this.form.group({
        nombre: [this.contacto.nombre,Validators.required],
        apellido: [this.contacto.apellido,Validators.required],
        empresa: [this.contacto.empresa,Validators.required],
      });
    }else{

      this.formularioContacto = this.form.group({
        nombre: ['',Validators.required],
        apellido: ['',Validators.required],
        empresa: ['',Validators.required],
      });
    }
    
    this.formularioTelefono = this.form.group({
      telefono: ['',Validators.required],
      lada: '+52',
      tipo: 'Personal',
      listaTelefonos: '',
    });
    
    this.formularioEmail = this.form.group({
      email: ['',Validators.required],
      proveedor: 'Google',
      tipo: 'Personal',
      listaCorreos: '',
    });
    
    // this.formularioDireccion = this.form.group({
    //   calle: ['',Validators.required],
    //   numero_externo: ['',Validators.required],
    //   numero_interno: ['',Validators.required],
    //   colonia: ['',Validators.required],
    //   municipio: ['',Validators.required],
    //   estado: ['',Validators.required],
    //   pais: ['',Validators.required],
    //   codigo_postal: ['',Validators.required],
    // });
    
  }

  ngOnInit(){
    if(this.id){
      this.dataService.getData('/api/contacto/' + this.id).subscribe(data => {
        this.contacto = data;
        this.contacto = this.contacto.contacto;
        console.log(this.contacto);
        
        var {nombre, apellido, empresa} = this.contacto[0];
        this.formularioContacto.setValue({nombre,apellido,empresa});

        var listaTelefonos = this.contacto.reduce((acc:any,x:any)=>{
          acc += acc == '' ? x.telefono : ';'+x.telefono;
          return acc;
        }, '');

        this.formularioTelefono.setValue({telefono:'', lada:'+52', tipo:'Personal', listaTelefonos});

      });
    }
  }

  hasErrors(controlName:string, errorType:string){
    return this.formularioContacto.get(controlName)?.hasError(errorType) && this.formularioContacto.get(controlName)?.touched;
  }

  saveContacto(){
    if(this.id){
      this.dataService.patchData('/api/contacto/'+this.id,this.formularioContacto.value).subscribe(data => {
        console.log(data);
        this.router.navigate(['/']);
      });
    }else{

      this.dataService.postData('/api/contacto',this.formularioContacto.value).subscribe(data => {
        console.log(data);
        this.router.navigate(['/']);
      });
    }
  }

  agregarTelefono(){
    console.log(this.formularioTelefono.value);
    const regex = /^(\+\d{1,2}\s?)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/g;
    var {telefono, listaTelefonos} = this.formularioTelefono.value;

    var validator = regex.exec(telefono);
    
    if(!validator){
      console.log('Ups! Algo salio mal!');
      return;
    }

    listaTelefonos += listaTelefonos=='' ? telefono: ';'+ telefono;
    telefono = '';
    this.formularioTelefono.setValue({listaTelefonos, telefono, lada:'+52', tipo:'Personal'});
  }
  
  saveTelefono(){
    console.log(this.formularioTelefono.value);

    if(this.id){
      var {listaTelefonos, lada, tipo} = this.formularioTelefono.value;
      var telefonos = listaTelefonos.split(';');
      for(let tel of telefonos){
        let data ={
          telefono:tel,
          lada,
          tipo,
          contacto_id:Number(this.id)
        }
        this.dataService.postData('/api/telefono',data).subscribe(data => console.log(data));
      }

    }
  }
  
  saveEmail(){
    // console.log(this.formularioContacto.value);
    this.dataService.postData('/api/contacto',this.formularioEmail.value).subscribe(data => {
      console.log(data);
      // this.router.navigate(['/']);
    });
  }
  
  // saveDireccion(){
  //   // console.log(this.formularioContacto.value);
  //   this.dataService.postData('/api/contacto',this.formularioDireccion.value).subscribe(data => {
  //     console.log(data);
  //     // this.router.navigate(['/']);
  //   });
  // }

}
