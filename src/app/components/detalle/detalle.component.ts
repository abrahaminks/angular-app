import { Component, Input, OnInit } from '@angular/core';
import { DataService } from '../../service/data.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-detalle',
  standalone: true,
  imports: [],
  providers:[DataService],
  templateUrl: './detalle.component.html',
  styleUrl: './detalle.component.css'
})
export class DetalleComponent implements OnInit {

  contacto:any;
  @Input('id') id = '';

  constructor(private dataService:DataService, private route:ActivatedRoute){}

  ngOnInit(){
    console.log(this.id);
    this.getContacto();
  }

  getContacto(){
    this.dataService.getData('/api/contacto/'+this.id).subscribe(data => {
      this.contacto = data;
      this.contacto = this.contacto?.contacto[0];
    });
  }

}
