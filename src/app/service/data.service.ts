import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http:HttpClient) {}

  getData(apiRoute:string){
    return this.http.get('http://127.0.0.1:8000'+apiRoute);
  }

  postData(apiRoute:string, body:any){
    return this.http.post('http://127.0.0.1:8000'+apiRoute, body);
  }
  
  putData(apiRoute:string, body:any){
    return this.http.put('http://127.0.0.1:8000'+apiRoute, body);
  }
  
  patchData(apiRoute:string, body:any){
    return this.http.patch('http://127.0.0.1:8000'+apiRoute, body);
  }
  
  deleteData(apiRoute:string, body:any=null){
    return this.http.delete('http://127.0.0.1:8000'+apiRoute, body);
  }
}
