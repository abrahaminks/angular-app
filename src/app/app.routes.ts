import { Routes } from '@angular/router';
import { ContactosComponent } from './components/contactos/contactos.component';
import { FormularioComponent } from './components/formulario/formulario.component';
import { DetalleComponent } from './components/detalle/detalle.component';

export const routes: Routes = [
    {path:'', component:ContactosComponent},
    {path:'formulario', component:FormularioComponent},
    {path:'formulario/:id', component:FormularioComponent},
    {path:'detalle/:id', component:DetalleComponent},
];
